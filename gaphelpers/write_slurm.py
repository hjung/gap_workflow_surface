import pandas as pd
import argparse, os
from pathlib import Path
from get_argparse import get_argparse


def get_adsorbate_info(mol, 
					mol_json= str(Path(__file__).parent.resolve()) + "/n3_mol_mod.json"):

	mdf = pd.read_json(mol_json)
	
	if isinstance(mol, int):
		idx = mol
		formula = mdf.loc[mol].formula
		smiles = mdf.loc[mol].smiles


	elif mol.isnumeric():
		mol = int(mol)
		idx = mol
		formula = mdf.loc[mol].formula
		smiles = mdf.loc[mol].smiles

	elif isinstance(mol, str):
		idx = mdf.loc[mdf.smiles == mol].index.item()
		formula = mdf.loc[mdf.smiles == mol].formula.item()
		smiles = mol

	return idx, formula, smiles


def write_slurm(
			end_iter,
			mol, 
			facet,
			iteration=0,
			forcemask=True,
			no_universal_soap=True,
			ncpus = 1,
			slurm_file_name = "submit.sh",
			mol_json = str(Path(__file__).parent.resolve()) + "/n3_mol_mod.json",
			code = "qe"
			):
	"""	
	This function write slurm submit file for Global optimization 
	Default file name is submit.sh 
	
	Parameters : 
	------------
	end_iter : int
		Maximum iteration for GAP fitting. (Usually less than 30 is more than enough in our study)
	mol : int or str  
		Either integer if this is already included in dataset in JSON file, else SMILES string is required. 
	facet : string
		Surface facet for surface either 111 or 211 (for now)
	iteration : int
		Iteration number 
	forcemask : bool
		whether to use force mask for lowest three metal layer.
	multiple_universal_soap : bool 
		whether to use multiple SOAP as give by universal SOAP
	ncpus : interger
		Number of cpu to be used when GAP fitting. Triggers multithreaded fitting. 
	slurm_file_name : str	
		name for submission bash file 
	mol_json : str 
		path for molecular dataset JSON file. 	
		131 molecules of [CHO]3 from 'ACS Omega 2019, 4, 3370−3379' were tested for this work
		Information on these molecules were gathered for simplicity. 
		However, presence of this JSON file is not mandatory. 
	"""
	
	
	idx, formula, smiles = get_adsorbate_info(mol)
	universal_soap = "-mus " if no_universal_soap else ""
	forcemask = "-fm " if forcemask else ""
	code = "-qe" if code == "qe" else ""

	slurm_str = f"""#!/bin/bash -l
#SBATCH -o ./tjob.out.%j
#SBATCH -e ./tjob.err.%j
#SBATCH -D ./
#SBATCH -J GAP_M{idx}_{formula}_{facet}
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task={ncpus}	
#SBATCH --mem=12000
#SBATCH --time=16:00:00

module purge

module load gcc/10
module load intel/19.1.2
module load impi/2019.8
module load mkl/2020.2
module load anaconda/3/2020.02

ulimit -s unlimited 
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
eval "$(conda shell.bash hook)"
conda activate quip

training_gap_iter_n.py -i {iteration} -e {end_iter} -mh {smiles} -f {facet} {universal_soap}{forcemask} {code} | tee -a stdout.log
"""


	with open(slurm_file_name, "w") as f:
		f.write(slurm_str)




def write_dft_slurm(jobname, 
					target_file_name = "structure.traj",
					slurm_file_name = "submit.sh",
					forcemask=False,
					code = "aims"
):

	forcemask = "-fm" if forcemask else ""
	code_name = "-qe" if code == "qe" else ""
	
	if code == "qe":
		add_string = 'export ASE_ESPRESSO_COMMAND="srun /u/hjung/Softwares/QE/qe-7.0/bin/pw.x -in PREFIX.pwi > PREFIX.pwo"\n'
#		add_string += 'export ESPRESSO_PSEUDO="/u/hjung/Softwares/QE/qe-7.0/pseudo/"'
	else:
		add_string = ""

	slurm_str = f"""#!/bin/bash -l
#SBATCH -o ./tjob.out.%j
#SBATCH -e ./tjob.err.%j
#SBATCH -D ./
#SBATCH -J {jobname}
#SBATCH --nodes=1
#SBATCH --ntasks=40
#SBATCH --ntasks-per-core=1
#SBATCH --mem=40000
#SBATCH --time=03:00:00

module purge

module load intel/19.1.2
module load impi/2019.8
module load mkl/2020.2
module load anaconda/3/2020.02

ulimit -s unlimited
export OMP_NUM_THREADS=1
export MKL_DYNAMIC=FALSE
export MKL_NUM_THREADS=1
eval "$(conda shell.bash hook)"
conda activate quip
{add_string}

##p_job=$(pwd -L | cut -d "/" -f 3-)
p_job=$(pwd -P)
randdir=`tr -dc A-Za-z0-9 </dev/urandom | head -c 13 ; echo ''`
p_scratch="/ptmp/`echo $p_job |cut -d '/' -f 4-`/$randdir/"
mkdir -p $p_scratch

cp -r * $p_scratch
cd $p_scratch

ase_submit.py {code_name} -sp {target_file_name}

# custom post-command stuff
cp -r *.traj espresso.* *.log $p_job
cd $p_job
"""
	with open(slurm_file_name, "w") as f:
		f.write(slurm_str)



if __name__ == "__main__":
	args = get_argparse()

	idx, formula, smiles = get_adsorbate_info(args.minimahopping)	
	print(idx, formula, smiles)
#	numdir = len(next(os.walk('.'))[1])
	f_name = f"{str(idx).zfill(3)}_M{idx}_{formula}_{args.facet}"
	Path(f_name).mkdir(parents=True, exist_ok=True)
	os.chdir(f_name)

	write_slurm(
			iteration = args.iteration, 
			end_iter = args.enditeration,
			mol = smiles, 
			facet = args.facet, 
			)	
	if args.submit:
		os.system("sbatch submit.sh")

