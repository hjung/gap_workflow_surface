#!/usr/bin/env python

from ase.calculators.aims import Aims
from ase.optimize import BFGS, FIRE

from ase import Atoms, atom
from ase.optimize.minimahopping import MinimaHopping
from ase.constraints import FixAtoms, Hookean, FixBondLengths
import ase.io
from ase.io import read, write
from ase.build import molecule
try:
	from quippy.potential import Potential
except:
	pass
from ase import neighborlist
from ase.visualize import view
from ase.optimize.minimahopping import MHPlot

import pandas as pd
import numpy as np
import os, sys, igraph as ig 
import argparse, pickle
from rdkit import Chem
from rdkit.Chem import AllChem 
from rdkit.Chem import rdmolops
from io import StringIO

def get_argparse():
	parser = argparse.ArgumentParser(description = "Give in input")
	parser.add_argument("-dr","--DFTrelaxation", nargs="?",type=str, const="adsorption.traj", help = "Perform geometry relaxation with DFT: max force 0.05 eV/A as default.")
	parser.add_argument("-sp","--DFTsinglepoint", nargs="?",type=str, const="adsorption.traj", help = "Perform single point calculation with DFT. default filename: adsorption.traj")
	# parser.add_argument("-sp","--singlepoint", action="store_true", help="Perform single point calculation")
	parser.add_argument("-mh","--minimahopping", action="store_true", help="Perform minima hopping with GAP. Default filename: adsorption.traj")
	parser.add_argument("-smi","--smiles", nargs="?", const=None, default=None, type=str, help="Impose Hookean constraint to molecule. Rcuts are inferred from given SMILES.")
	parser.add_argument("-rm","--relaxmetal", action="store_true", help="Relax upper two metal layers. If not used, all metal slab is constrained")
	parser.add_argument("-p", "--potential", nargs="?",type=str, const="GAP_0.xml", help="filename of GAP potential")
#	parser.add_argument("-ic", "--inherent_constraint", action="store_true", help="Use constraint as stored in *.traj file" )
	parser.add_argument("-ts", "--twostep", action="store_true", help="Use constraint as stored in *.traj file and toggle two step DFT relaxation" )
	parser.add_argument("-sd", "--submitdir", nargs="?",type=str,default="", help="path submit directory")
	parser.add_argument("-r", "--resume", action="store_true", help="Resume DFT relaxation (esp. with two step relaxation)")
	return parser.parse_args()


def graphfromsmiles(smiles):
    """
    Generate igraph object from smiles
    Convert 2D smiles into 3D geometry using embedding and subsequently optimize with MMFF
    From MMFF optimized geometry, bondlengths, etc are saved as graph edge attributes. 
    This will be used for graph matching from ASE read adsorbate's molecular graph. 
    
    Parameters : 
    ------------
    smiles : str
        SMILES string for adsorbate
        
    Return : 
    --------
    molgraph : igraph object
        graph of molecule with bondtype, bondlengths, etc, are encoded as edge attributes
    """

    m  = Chem.AddHs(Chem.MolFromSmiles(smiles))
    AllChem.EmbedMolecule(m)
    AllChem.MMFFOptimizeMolecule(m)
    position = m.GetConformer().GetPositions()

    # initiate molecular graph
    molgraph = ig.Graph.Adjacency(rdmolops.GetAdjacencyMatrix(m), mode = "undirected")
    molgraph.vs["Symbols"] = [m.GetAtomWithIdx(i).GetSymbol() for i in range(m.GetNumAtoms())]
    molgraph.vs["AtomicNums"] = [m.GetAtomWithIdx(i).GetAtomicNum() for i in range(m.GetNumAtoms())]


    for bond in m.GetBonds(): 

        bondidxtuple = bond.GetBeginAtomIdx(), bond.GetEndAtomIdx()    

        molgraph.es[molgraph.get_eid(*bondidxtuple)]["bondtype"] = str(bond.GetBondType())
        molgraph.es[molgraph.get_eid(*bondidxtuple)]["atomsymbols"] = tuple([m.GetAtomWithIdx(i).GetSymbol() for i in bondidxtuple])
        molgraph.es[molgraph.get_eid(*bondidxtuple)]["bondlength"] = np.linalg.norm(position[bondidxtuple[0]] - position[bondidxtuple[1]])
        
    return molgraph

def graphfromase(slabi, metal = "Rh"):
    """
    For given surface slab with adsorbate, this first calculate adjacency matrix and extract
    adsorbate's molecular graph as adjacency matrix. 
    This is returned as igraph object
    
    Parameters :
    ------------
    slabi : ase object
        metal surface with adsorbate on top. 
    metal : string
        metal element symbol. 
        
    Return : 
    --------
    ase_graph : igraph object
        adsorbate molecule's graph object
    """
    cutOff = neighborlist.natural_cutoffs(slabi) # generate covalent radii for each elements in ase object
    # neighbor = 
    neighbor = neighborlist.NeighborList(cutOff, self_interaction=False, bothways=True)
    neighbor.update(slabi)

    atom_dict = {i : symb for i, symb in enumerate(slabi.get_chemical_symbols()) if symb != metal}
    adsorbateidx = sorted(list(atom_dict.keys()))[0]
    adsorbateidx
    
    
    adjacency = neighbor.get_connectivity_matrix(sparse=False)[adsorbateidx:, adsorbateidx:]
    ase_graph = ig.Graph.Adjacency(adjacency, mode = "undirected")
    ase_graph.vs["Symbols"] = [atom.symbol for atom in slabi[sorted(atom_dict.keys())]]
    ase_graph.vs["AtomicNums"] = [atom.number for atom in slabi[sorted(atom_dict.keys())]]
    ase_graph.vs["AtomIndex"] = sorted(list(atom_dict.keys()))
    
    return ase_graph

def check_adsorbate_isomorphism(smiles, slab, metal = "Rh"):
    """
    Compare two molecular graph from smiles and from slab structure, and 
    encode expected bondlength and other properties to slab structure. 
    This informaiton will be further used to generate Hookean constraints. 
    
    Parameters :
    ------------
    smiles : str
        SMILES string for adsorbate
    slab : ase object
        metal surface with adsorbate on top. 
        
    Return : 
    ase_graph : igraph object
        with bond length saved as attribute, adsorbate graph is returned is isomorphism match 
        was successful. If failed, False is returned. 
    --------
    
    """
    molgraph = graphfromsmiles(smiles)
    
    ase_graph = graphfromase(slab)
    
    isomorphic, map12, map21 = molgraph.isomorphic_vf2(ase_graph, 
                        color1=molgraph.vs["AtomicNums"], 
                        color2=ase_graph.vs["AtomicNums"],
                       	return_mapping_12 = True, return_mapping_21 = True)

    mapping = dict(zip(map12, map21))
    # print(isomorphic, mapping)

    if isomorphic:
        for smiles_edge in molgraph.es:
            ase_graph.es[ase_graph.get_eid(*tuple([mapping[idx] for idx in smiles_edge.tuple]))].update_attributes(smiles_edge.attributes())
            
        return ase_graph
    else:
        raise NameError("error: not isomorphic!")
        
        return False

    
def apply_constraints(atoms, **kwargs):

	smiles = kwargs.get("smiles", None)
	relax_metal = kwargs.get("relax_metal", False)
	constrain_Hookean = kwargs.get("constrain_Hookean", True)
	
	if constrain_Hookean:
		# Remove only FixAtom on metal 
		for constrain in atoms.constraints:
			if str(constrain).startswith("FixAtoms"):
				remove = constrain
		atoms.constraints.remove(remove)
		cons = atoms.constraints

	else:
		del atoms.constraints  # Delete constraints first
		cons = []        


	if relax_metal:
		height_set = sorted(set([atom.z for atom in atoms if atom.symbol == "Rh"]))
		if len(height_set) == 4:
			uppermost_layer_height = height_set[-2]
		elif len(height_set) == 12:
			uppermost_layer_height = height_set[-6]
		else:
			uppermost_layer_height = np.histogram([atom.z for atom in atoms if atom.symbol == "Rh"], bins=12)[1][6]

		cons.append(FixAtoms(indices=[atom.index for atom in atoms if atom.z < uppermost_layer_height]))
	else:
		cons.append(FixAtoms(indices=[atom.index for atom in atoms if atom.symbol == 'Rh']))

	if constrain_Hookean and smiles != None:
		mapped_graph = check_adsorbate_isomorphism(smiles, atoms)
		
		if mapped_graph:
			for edge in mapped_graph.es:
				bond_pair = [mapped_graph.vs[vid]["AtomIndex"] for vid in edge.tuple]
				cons.append(Hookean(a1 = bond_pair[0], a2 = bond_pair[1], 
						rt = 1.05*edge["bondlength"], k = 20))
		
	else:
		pass
		
	atoms.set_constraint(cons)


args = get_argparse()


if args.DFTrelaxation:

	filename, extension = os.path.splitext(args.DFTrelaxation)
	if extension == ".pickle":
		with open(args.DFTrelaxation, "rb") as h:
			atoms = pickle.load(h)
	else:
		atoms = read(args.DFTrelaxation, index="-1")	

#	if not args.twostep:
	apply_constraints(atoms,
		relax_metal=args.relaxmetal,
		smiles = args.smiles,
		constrain_Hookean = True
		)
	

	print(atoms.constraints)

	aimsbin ="/u/hjung/Softwares/FHIaims2021/build/aims.210716_1.scalapack.mpi.x"
	mpiexe = "srun"
	outfile = f"{args.submitdir}/stdout.log" if args.submitdir else "stdout.log"
	aims_command = "{} {} > {}".format(mpiexe, aimsbin, outfile)
	#print(aims_command)
	calc = Aims(
		command = aims_command,
		charge=0.0,
		species_dir='/u/hjung/Softwares/FHIaims2021/species_defaults/light_vdW_surf',
		xc='revpbe',
		vdw_correction_hirshfeld='.true.',
		spin='none',
		relativistic='atomic_zora scalar',
		output=['hirshfeld'],
		occupation_type='gaussian 0.1',
		vdw_pair_ignore='Rh Rh',
		k_grid = "4 4 1",
		sc_accuracy_forces=1e-4,
		sc_accuracy_etot=1e-5,
		sc_accuracy_eev=1e-3,
		sc_accuracy_rho=1e-4
	)

	if args.twostep and args.resume:

		atoms.calc = calc
		os.rename("bfgs_opt2.traj", "bfgs_opt2_prev.traj")
		opt = FIRE(atoms, trajectory = "bfgs_opt2_re.traj")
		opt.run(fmax = 0.05)
		os.system("ase gui bfgs_opt2_prev.traj bfgs_opt2_re.traj -o bfgs_opt2.traj")
		os.system("ase gui bfgs_opt1.traj bfgs_opt2.traj -o bfgs_opt.traj")
	
	elif args.twostep:

		atoms.calc = calc	
		opt = FIRE(atoms, trajectory = "bfgs_opt1.traj")

		opt.run(fmax = 0.2)

		print("DFT Relaxation with Hookean constraint finished")
		# Optimization without Hookean Constraint  
		newcons = [constraint for constraint in atoms.constraints if str(constraint).startswith("FixAtoms")]
		atoms.set_constraint(newcons)
		print(atoms.constraints)

		opt = FIRE(atoms, trajectory = "bfgs_opt2.traj")
		opt.run(fmax = 0.05)
		os.system("ase gui bfgs_opt1.traj bfgs_opt2.traj -o bfgs_opt.traj")
	
	else:
		atoms.calc = calc	
		opt = FIRE(atoms, trajectory = "bfgs_opt.traj")
		opt.run(fmax = 0.05)


elif args.DFTsinglepoint:

	filename, extension = os.path.splitext(args.DFTsinglepoint)
	if extension == ".pickle":
		with open(args.DFTsinglepoint, "rb") as h:
			atoms = pickle.load(h)
	else:
		structures = read(args.DFTsinglepoint, index=":")

		if len(structures) > 1:
			atoms = structures[-1]
		else:
			atoms = structures[0]

	aimsbin ="/u/hjung/Softwares/FHIaims2021/build/aims.210716_1.scalapack.mpi.x"
	mpiexe = "srun"
	outfile = f"{args.submitdir}/stdout.log" if args.submitdir else "stdout.log"
	aims_command = "{} {} > {}".format(mpiexe, aimsbin, outfile)
	calc = Aims(
		command = aims_command,
		charge=0.0,
		species_dir='/u/hjung/Softwares/FHIaims2021/species_defaults/light_vdW_surf',
		xc='revpbe',
		vdw_correction_hirshfeld='.true.',
		spin='none',
		relativistic='atomic_zora scalar',
		output=['hirshfeld'],
		occupation_type='gaussian 0.1',
		vdw_pair_ignore='Rh Rh',
		k_grid = "4 4 1",
		sc_accuracy_forces=1e-4,
		sc_accuracy_etot=1e-5,
		sc_accuracy_eev=1e-3,
		sc_accuracy_rho=1e-4
	)

	atoms.calc = calc
	atoms.get_potential_energy(force_consistent=True)


elif args.minimahopping:


	# Read in Structure
	atoms = read("adsorption.traj", index="-1")

	# Set Constraints 
	apply_constraints(atoms, 
			relax_metal=args.relaxmetal, 
			smiles=args.smiles)

	print(atoms.constraints)

	# Set Calculator/ Potential
	print("start reading potential file")
	#pot_file = args.potential
	pot = Potential(param_filename = args.potential)
	atoms.set_calculator(pot)
	print(f"Potential : {args.potential}")
	print("potential file has been read!")

	# Minima Hopping
	hop = MinimaHopping(atoms, Ediff0=0.75, T0=2000., minima_traj="../minima.traj")
	hop(totalsteps = 20)

	mhplot = MHPlot(E0=True)
	mhplot.save_figure('MinHop_summary.png')

