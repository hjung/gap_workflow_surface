#!/bin/sh
''''exec python -u -- "$0" ${1+"$@"} # '''
# vi: syntax=python

# This script can be used to train a GAP potential
# This is modified by Hyunwook @ 28th Jan 2022 on Raven

from ase.io import read, write
import os, sys, pandas as pd, numpy as np, glob, pickle
from shutil import copyfile, rmtree 
#from gaphelpers.sample_training_set import sample_training_set, check_slurm_completion, prepare_initial_config
from gaphelpers.gap_command import gap_fitting, get_validation, plot_accuracy 
from gaphelpers.minimahopping import minimahopping, prepare_input, check_GAP_convergence, Run_parallel_minima_hopping, check_GAP_convergence_after_parallel_MH, sample_training_set, check_slurm_completion, prepare_initial_config
from gaphelpers.get_argparse import get_argparse
from gaphelpers.encode import make_trainingset_file, parse_output
from gaphelpers.write_slurm import get_adsorbate_info, write_dft_slurm
#from gaphelpers.hyperparameter import get_atomization_energy
from gaphelpers.cluster import cluster_sample
from pathlib import Path
import time
#from sklearn.metrics import mean_squared_error as rmse

args = get_argparse()
################################
scratchfolder = "/ptmp" + os.getcwd()[8:] 
submitdir = os.getcwd()
# make temp folder
if args.noscratch:
	tmpdir = submitdir
else:
	tmpdir = scratchfolder
Path(tmpdir).mkdir(parents=True, exist_ok = True)
################################

# Automatic determination of starting iteration
# if there's no proceeded training iteration
last_iter = sorted([fn for fn in next(os.walk(submitdir))[1] if fn.startswith("iter_")], key =lambda s:int(s.split("_")[1]))[-1] 
last_iter = int(last_iter.split("_")[1])
if len([fn for fn in next(os.walk(submitdir))[1] if fn.startswith("iter_")]) == 0:
	start_iter = int(args.iteration)
elif last_iter != int(args.iteration):
	print(f"last iteration was {last_iter}. Resume workflow from there. ")
	start_iter = last_iter + 1
else:
	start_iter = int(args.iteration)


if args.enditeration == None:
    end_iter = start_iter
else:
	end_iter = int(args.enditeration)


if args.quantum_espresso:
	lattice_param, vacuum, code = 3.86, 16, "qe"
else:
	lattice_param, vacuum, code = 3.85, 30, "aims"

if start_iter == 0 and not os.path.isfile(submitdir + "/input_training_data_iter_0.xyz"):
	prepare_initial_config(args.minimahopping, surface = args.facet, forcemask = args.force_mask, lattice_param=lattice_param, vacuum=vacuum, code=code)

if start_iter != 0 and os.path.isfile(submitdir + "/statistics.csv"):
	statistics = pd.read_csv(submitdir + "/statistics.csv", index_col=0).transpose().to_dict()
#elif calc_all_minima:
#	statistics = pd.read_csv(submitdir + "/all_minima_statistics.csv", index_col=0).transpose().to_dict()
else:
	statistics = {}

timing = {}

# ===========================
# Start Iterative GAP fitting
# ===========================
for iteration in range(start_iter, end_iter+1):

	if iteration >= 5 and check_GAP_convergence(statistics, submitdir, sampling_method = args.sampling, calc_all_minima = args.calc_all_minima, convergence=args.convergence):
		break	

	os.chdir(tmpdir)
	Path( tmpdir + f'/iter_{iteration}').mkdir(parents=True, exist_ok = True)
	if iteration == 0:
		copyfile(submitdir + f'/input_training_data_iter_0.xyz',
				tmpdir + f'/iter_{iteration}/input_training_data_iter_{iteration}.xyz')
	else:
		os.system(f"rsync -az {submitdir}/iter_{iteration-1}/3_DFT_minhop/input_training_data_iter_{iteration}.xyz " +
				  f"{tmpdir}/iter_{iteration}/input_training_data_iter_{iteration}.xyz")

	# GAP fitting
	#os.environ["OMP_NUM_THREADS"] = "36"
	f_name = f"{submitdir}/iter_{iteration}/GAP_2b_soap_iter_{iteration}"
	if os.path.isdir(f"{f_name}") and os.path.isfile(f"{f_name}/GAP_2b_soap_iter_{iteration}.xml"):
		# Replacement of segmentation error
		print("GAP_fitting is already done! Resuming to minima hopping")
		reference_dat = read(tmpdir + f'/iter_{iteration}/input_training_data_iter_{iteration}.xyz@:')
		quip_results = read(tmpdir + f"/iter_{iteration}/GAP_2b_soap_iter_{iteration}/quip_train_GAP_2b_soap_iter_{iteration}.xyz@:")
		statistics[iteration] = plot_accuracy(iteration, reference_dat, quip_results, 'GAP_2b_soap_', code=code)

	else:
		start = time.time()
		statistics[iteration] = gap_fitting(iteration, tmpdir, forcemask=args.force_mask,
											multiple_universal_soap = args.multiple_universal_soap, code=code)  
		timing["gapfit"] = time.time() - start


	# Minima hopping
	#os.environ["OMP_NUM_THREADS"] = "1"
	start = time.time()
	prepare_input(iteration=iteration, smiles=args.minimahopping,
			facet=args.facet, tmpdir=tmpdir, randseed=iteration, lattice_param=lattice_param, vacuum=vacuum)

	minimahopping(iteration, args.minimahopping, 
				f"../GAP_2b_soap_iter_{iteration}/GAP_2b_soap_iter_{iteration}.xml",
				facet = args.facet,
				tmpdir = tmpdir,
				randseed = iteration,
				statistics = statistics, 
				timestep = 0.5,
				relax_metal = args.relaxmetal)
	timing["minhop"] = time.time() - start



	# DFT calculation
	os.chdir(submitdir)
	if tmpdir != submitdir:
		os.system(f"rsync -az {tmpdir}/iter_{iteration} {submitdir}")
		rmtree(tmpdir + f"/iter_{iteration}")	

	start = time.time()
	job_ids = sample_training_set(iteration, forcemask=args.force_mask, sampling_method = args.sampling, parallel=args.parallel,calculate_all_minima = args.calc_all_minima, code=code )

   
	print("DFT job submitted.")
	if check_slurm_completion(job_ids):

		if args.calc_all_minima:
			for j in range(len(read(f"{submitdir}/iter_{iteration}/2_Minhop/minima.traj@:"))):
				print(j, end = " ")
				os.chdir(f"{submitdir}/iter_{iteration}/2_Minhop/{j}_minima")
				atoms = parse_output()
				atoms.arrays["forces_dft"] = atoms.get_forces()
				write(f"{submitdir}/iter_{iteration}/2_Minhop/input_training_data_iter_{iteration+1}.xyz",
					  atoms, format="extxyz", append="w")

		print("DFT job all finished")

	make_trainingset_file(iteration, submitdir, forcemask=args.force_mask, code=code)

	timing["DFT"] = time.time() - start
	

	# Validation (GAP Evaluation)
	start = time.time()
	validation_error = get_validation(iteration, submitdir, calc_all_minima = args.calc_all_minima, code=code)
	if iteration in statistics.keys(): 
		statistics[iteration] = {**statistics[iteration], **validation_error}
	else:
		statistics[iteration] = validation_error

	timing["validation"] = time.time() - start
	
	print(statistics[iteration])
	
	if os.path.isfile(submitdir + "/statistics.csv"):
		pd.DataFrame.from_dict({iteration : statistics[iteration]}).transpose().to_csv(submitdir + "/statistics.csv", mode="a", header=False)
	else:
		pd.DataFrame.from_dict({iteration : statistics[iteration]}).transpose().to_csv(submitdir + "/statistics.csv")

	for key, val in timing.items():
		print(f"iter #{iteration} {key} : {val:.2f} s ")
	print(f"iter #{iteration} total : {sum(list(timing.values())):.2f} s" )




# =======================
# Parallel Minima hopping
# =======================
Run_parallel_minima_hopping(iteration, submitdir, args.minimahopping, args.facet, lattice_param=lattice_param, vacuum=vacuum, relax_metal=args.relaxmetal)



# ===============
# DFT Relaxation
# ===============

#if not check_GAP_convergence_after_parallel_MH(args.minimahopping, submitdir, code=code):
#	print(f"Error for selected five minima is too high. ")
#
#	# In case you don't know what's the iteration number
#	iteration = int(sorted(glob.glob("iter_*"), key=lambda x:int(x.split("_")[-1]))[-1].split("_")[-1]) + 1
#	# copy xyz file to current folder
#	os.system(f"rsync -az {submitdir}/iter_{iteration-1}/3_DFT_minhop/input_training_data_iter_{iteration}.xyz "
#			  f"{submitdir}/c_DFT_singlepoint4cluster/input_training_data_iter_{iteration}.xyz") 
#	make_trainingset_file(iteration, submitdir, forcemask=args.force_mask, update="patch", code=code)
#		
#	os.chdir(tmpdir)
#	Path( tmpdir + f'/iter_{iteration}').mkdir(parents=True, exist_ok = True)
#	os.system(f"rsync -az {submitdir}/c_DFT_singlepoint4cluster/input_training_data_iter_{iteration}.xyz " +
#			   f"{tmpdir}/iter_{iteration}/input_training_data_iter_{iteration}.xyz")
#	statistics[iteration] = gap_fitting(iteration, tmpdir, forcemask=args.force_mask,
#										multiple_universal_soap = args.multiple_universal_soap, code=code)
#	pd.DataFrame.from_dict({iteration : statistics[iteration]}).transpose().to_csv(submitdir + "/statistics.csv", mode="a", header=False)
#
#
#	Run_parallel_minima_hopping(iteration, submitdir, args.minimahopping, args.facet, rerun = True, lattice_param=lattice_param, vacuum=vacuum, relax_metal=args.relaxemetal)
#	cluster_sample(args.minimahopping, submitdir=submitdir, only_chemisorption=True, code=code)	
#
#else:
cluster_sample(args.minimahopping, submitdir=submitdir, only_chemisorption=True,  code = code)	

