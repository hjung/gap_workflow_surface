# Machine-Learning Driven Global Optimization of Surface Adsorbate Geometries
authored by : Hyunwook Jung, Lena Sauerland, Sina Stocker, Karsten Reuter, Johannes T. Margraf

Fritz-Haber-Institut der Max-Planck-Gesellschaft Faradayweg 4-6 D-14195 Berlin Germany

# Overview
This workflow package aims for accelerating global optimization of surface adsorbate structure with active training of machine-learning potential where Gaussian Approximation Potential (GAP) is used herein. Surrogate GAP model is fitted on-the-fly starting from single structure and iterative training scheme automatically selects required training set structures and hyperparameters  

This workflow entails DFT calculation where FHI-aims is used in current implementation. Therefore this requires FHI-aims to be installed but extension to other software is also possible. In addition to DFT code, this utilize GAP within QUIP interface, parallel minima hopping using GNU parallel, RDkit, universal SOAP, DScribe. Therefore corresponding settings are required. 



# Settings:
1. scratchfolder : If you are using scratchfolder, you can specify scratch folder to use. otherwise, You can avoid using separate scratch folder by adding optional keyword argument `-ns` or `--noscratch` when executing `training_gap_iter_n.py`
2. DFT submission : DFT single-point or relaxation is carried out by executing `ase_submit.py` file. This file can be placed such as binary directory `/bin` to make it executalbe. Otherwise, user should modify the file location in `write_slurm.py` in `gaphelpers` folder. 
3. The number of CPUs and specific settings in SLURM submission file should be adapted depending on specific computational environment of user. 

# Dependency: 
- [igraph](https://igraph.org/)
- [GNU parallel](https://www.gnu.org/software/parallel)
- [RDKit](https://www.rdkit.org)
- [QUIP](https://github.com/libAtoms/QUIP)
- [universal SOAP](https://github.com/libAtoms/universalSOAP)
- [DScribe](https://singroup.github.io/dscribe/latest/)


# License:
This package is released under the MIT License.

